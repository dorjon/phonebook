import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentReference } from '@angular/fire/firestore';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Contact } from '../contact.model';

@Injectable({
  providedIn: 'root'
})
export class PhonebookService {
  private contactList: Observable<Contact[]>;
  private contactListCollection: AngularFirestoreCollection<Contact>;
  
  constructor(private fireStore: AngularFirestore) {
    this.contactListCollection = this.fireStore.collection<Contact>('contacts');
    this.contactList = this.contactListCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data};
        });
      })
    );
   }

  getContactList(): Observable<Contact[]> {
    return this.contactList;
  }

  addContact(contact: Contact): Promise<DocumentReference> {
    // added static as image
    contact.imageUrl = `assets/icon/avatar${Math.floor(Math.random() * 10)}.svg`;
    // Adding timestamp of creation
    contact.timestamp = new Date().toString();
    return this.contactListCollection.add(contact);
  }

  getContact(id: string): Observable<Contact> {
    return this.contactListCollection.doc<Contact>(id).valueChanges().pipe(
      take(1),
      map(contact => {
        contact.id = id;
        return contact;
      })
    );
  }

  updateContact(id: string, contact: Contact): Promise<void> {
    return this.contactListCollection.doc(id).update(contact);
  }
 
  deleteContact(id: string): Promise<void> {
    return this.contactListCollection.doc(id).delete();
  }

}
