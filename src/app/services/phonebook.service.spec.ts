import { TestBed } from '@angular/core/testing';

import { PhonebookService } from './phonebook.service';
import { BehaviorSubject, of } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { Contact } from '../contact.model';

const FirestoreStub = {
  collection: (name: string) => ({
    doc: (_id: string) => ({
      valueChanges: () => new BehaviorSubject({ foo: 'bar' }),
      set: (_d: any) => new Promise((resolve, _reject) => resolve()),
      update: (contact: Contact) => new Promise((resolve, _reject) => resolve())
    }),
    snapshotChanges: () =>  new BehaviorSubject([{firstName: 'Dhiraj', lastName: 'Rane', description: 'hello this is test'}])
  }),
};

describe('PhonebookService', () => {
  let service: PhonebookService;

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [
      { provide: AngularFirestore, useValue: FirestoreStub },
    ],});
    service = TestBed.inject(PhonebookService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
