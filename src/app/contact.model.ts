export interface Contact {
  id?: string;
  firstName: string;
  lastName: string;
  description: string;
  phoneNumber?: number;
  email?: string;
  twitter?: string;
  imageUrl?: string;
  timestamp?: string;
}