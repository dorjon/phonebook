import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { Router, RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule, FormGroup } from '@angular/forms';
import { PhonebookService } from '../services/phonebook.service';

import { EditContactPage } from './edit-contact.page';
import { of } from 'rxjs';

const phoneBookServiceStub = {
  addContact() {
    return Promise.resolve();
  },
  getContact() {
    return of({firstName: 'Dhiraj', lastName: 'rane', description: 'Hello this is test'});
  },
  updateContact() {
    return Promise.resolve();
  },
  deleteContact() {
    return Promise.resolve();
  }
};

describe('EditContactPage', () => {
  let component: EditContactPage;
  let fixture: ComponentFixture<EditContactPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditContactPage ],
      imports: [IonicModule.forRoot(), RouterTestingModule, ReactiveFormsModule],
      providers: [
        {provide: PhonebookService, useValue: phoneBookServiceStub}
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(EditContactPage);
    component = fixture.componentInstance;
    
    component.ngOnInit();
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ionViewWillEnter', () => {
    spyOn(phoneBookServiceStub, 'getContact').and.returnValue(of({firstName: 'Dhiraj', lastName: 'rane', description: 'Hello this is test'}));
    component.id = '1233';
    component.ionViewWillEnter();
    fixture.detectChanges();
    expect(phoneBookServiceStub.getContact).toHaveBeenCalled();
  })

  it('addOrUpdateContact() - add contact', () => {
    spyOn(component, 'isNewContactAction').and.returnValue(true);
    spyOn(phoneBookServiceStub, 'addContact').and.returnValue(Promise.resolve());
    component.addOrUpdateContact();
    expect(phoneBookServiceStub.addContact).toHaveBeenCalled();
  })

  it('addOrUpdateContact() - update contact', () => {
    spyOn(component, 'isNewContactAction').and.returnValue(false);
    spyOn(phoneBookServiceStub, 'updateContact').and.returnValue(Promise.resolve());
    component.addOrUpdateContact();
    expect(phoneBookServiceStub.updateContact).toHaveBeenCalled();
  })

  it('deleteContact()', () => {
    spyOn(phoneBookServiceStub, 'deleteContact').and.returnValue(Promise.resolve());
    component.deleteContact();
    expect(phoneBookServiceStub.deleteContact).toHaveBeenCalled();
  })

  it('clearField()', () => {
    spyOn(component.form, 'patchValue').and.returnValue();
    component.clearField('phoneNumber');
    expect(component.form.patchValue).toHaveBeenCalled();
  })
});
