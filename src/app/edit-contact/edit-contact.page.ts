/* 
  This is the common page for Edit contact view and Create new contact view.
  Switching to two different views based on router url.
*/

import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PhonebookService } from '../services/phonebook.service';
import { Contact } from '../contact.model';

@Component({
  selector: 'app-edit-contact',
  templateUrl: './edit-contact.page.html',
  styleUrls: ['./edit-contact.page.scss'],
})

export class EditContactPage implements OnInit {
  backButtonText: string;
  form: FormGroup;
  contact: Contact;
  id = null;
  constructor(
    private activatedRoute: ActivatedRoute, 
    private router: Router, 
    private phoneBookService: PhonebookService,
    private toastController: ToastController
  ) {
     this.id = this.activatedRoute.snapshot.paramMap.get('id');
   }
  
  // Edit contact view - update form values
  ionViewWillEnter() {
    if (this.id) {
      this.phoneBookService.getContact(this.id).subscribe(contact => {
        this.form.patchValue({
          ...contact
        });
      });
    }
  }

  ngOnInit() {
    this.backButtonText = this.isNewContactAction() ? 'Save New Contact' : 'Edit Contact';
    this.form = new FormGroup({
      firstName: new FormControl(null, {
        validators: [Validators.required]
      }),
      lastName: new FormControl(null, {}),
      description: new FormControl(null, {}),
      phoneNumber: new FormControl(null, {
        validators: [Validators.pattern(/^-?(0|[1-9]\d*)?$/)]
      }),
      email: new FormControl(null, {}),
      twitter: new FormControl(null, {})
    });
  }

  isNewContactAction() {
    return this.router.url === '/new-contact';
  }

  // common function to add or update contact based on router url
  addOrUpdateContact() {
    if(this.isNewContactAction()) {
      this.phoneBookService.addContact(this.form.value).then(() => {
        this.router.navigateByUrl('/');
        this.showToast('Contact added');
      }, err => {
        this.showToast('There was a problem adding your contact');
      });
    } else {
      this.phoneBookService.updateContact(this.id, this.form.value).then(() => {
        this.router.navigateByUrl('/');
        this.showToast('Contact updated');
      }, err => {
        this.showToast('There was a problem updating your contact');
      });
    }   
  }

  deleteContact() {
    this.phoneBookService.deleteContact(this.id).then(() => {
      this.router.navigateByUrl('/');
      this.showToast('Contact deleted');
    }, err => {
      this.showToast('There was a problem deleting your contact');
    });
  }

  clearField(field: string) {
    this.form.patchValue({
      [field]: null
    });
  }

  showToast(msg: string) {
    this.toastController.create({
      message: msg,
      duration: 2000
    }).then(toast => toast.present(), err => {
      this.showToast('Sorry for inconvenience');
    });
  }
}
