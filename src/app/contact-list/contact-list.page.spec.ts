import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { RouterTestingModule } from '@angular/router/testing';

import { ContactListPage } from './contact-list.page';
import { of } from 'rxjs';
import { PhonebookService } from '../services/phonebook.service';


const phoneBookServiceStub = {
  getContactList() {
    const contacts = [{firstName: 'Dhiraj', lastName: 'Rane', description: 'hello this is test'}];
    return of( contacts );
  }
};

describe('ContactListPage', () => {
  let component: ContactListPage;
  let fixture: ComponentFixture<ContactListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactListPage ],
      imports: [IonicModule.forRoot(), RouterTestingModule],
      providers: [{provide: PhonebookService, useValue: phoneBookServiceStub}]
    }).compileComponents();

    fixture = TestBed.createComponent(ContactListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('contact list should get updated', () => {
    expect(component.contactList).toEqual([{firstName: 'Dhiraj', lastName: 'Rane', description: 'hello this is test'}]);
  });

  it('search contact', () => {
    component.searchContact({target: { value: 'Dhiraj'}});
    expect(component.searchContactList).toEqual([{firstName: 'Dhiraj', lastName: 'Rane', description: 'hello this is test'}]);
  })
});
