import { Component, OnInit } from '@angular/core';
import { Contact } from '../contact.model';
import { PhonebookService } from '../services/phonebook.service';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.page.html',
  styleUrls: ['./contact-list.page.scss'],
})
export class ContactListPage implements OnInit {
  contactList: Contact[];
  searchContactList: Contact[];
  isSortedBydate = false;
  constructor(private phoneBookService: PhonebookService) { }

  ngOnInit() {
    this.getContactList();
  }

  async getContactList() {
    this.phoneBookService.getContactList().subscribe(data => {
      this.contactList = data.sort((a, b) => a.firstName.localeCompare(b.firstName));
      this.searchContactList = [...this.contactList];
    });
  }

  searchContact(event) {
    this.searchContactList = this.contactList.filter(contact => {
      return contact.firstName.toLowerCase().includes(event.target.value.toLowerCase())
    });
  }

  sortByDateAdded() {
    this.isSortedBydate = !this.isSortedBydate;
    if(this.isSortedBydate) {
      this.searchContactList = this.contactList.sort((a,b) => {
        var key1 = Date.parse(a.timestamp);
        var key2 = Date.parse(b.timestamp);
    
        if (key1 < key2) {
            return -1;
        } else if (key1 == key2) {
            return 0;
        } else {
            return 1;
        }
      });
    } else {
      this.searchContactList = this.contactList.sort((a, b) => a.firstName.localeCompare(b.firstName));
    }
  }

  refresh(ev) {
    this.getContactList().then(() => ev.detail.complete());
  }
}
