# PhoneBook #

Phonebook application is desktop application which will help to maintain your contact details information.
This is Ionic application and will be deployed in Electron shell.

### How to run locally ionic application ###

* Clone this repository to your local workspace
* execute `npm install`
* To run ionic application on web execute `npm run start`
* To build ionic application `npm run build`
* To run Test `npm run test`

### How to deploy Ionic page to Electron ###
Here we will build a simple Ionic app with Capacitor and add Electron to finally build a native desktop out of our basic application.

Follow below steps to create electron desktop application using existing Ionic

* run `npm run build` -  Needed to run once before adding Capacitor platforms
* run to add electron platform with Ionic app `npm run add:electron`
* start electron app locally `npm run start:electron`
* create package for mac `npm run package:electron:mac`
* create package for win `npm run package:electron:win`
* create installer for mac `npm run package:mac:installer` this will create packages for both win and mac.
* You will find packages created under release-build folder
* you find installer created under dist folder


### Extra information

* Approach towards application [check](docs/approach.md)

* Accessibility coverage [check](docs/accessibility.md)

* you might face issue for generating windows package if you are using any other platform as Mac. If the issue is related to wine64 then you have to install `wine-devel`
