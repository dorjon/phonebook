## Approach

* First created blank ionic project with capacitor

  `ionic start phonebook blank --type=angular --capacitor`

* Created pages `contact-list` and `edit-contact` with `contact component`
* using `firebase` for storage
* for firebase connection used `angularfire`
* added `ngx-electron`, `electron` and `electron-packager` for deploying Ionic to Electron

Back to main [click](../README.md)
