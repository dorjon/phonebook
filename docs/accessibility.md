## Accessibility coverage

I have did basic accessibility check using WAVE ( web accessibility evaluation tool ).

Please check below coverage report

![coverage](accessibility_coverage.png)

Back to main [click](../README.md)